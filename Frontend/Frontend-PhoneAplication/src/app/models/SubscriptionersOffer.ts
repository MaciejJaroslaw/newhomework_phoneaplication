import { Activities } from './Activities';

export class SubscriptionersOffer {
    Id: number;
    IfOfOffer: number;
    IdOfSubscriptioner: number;
    NumberOfPhone: string;
    TimeOfAllConnections: number;
    AmmountOfAllMessage: number;
    CostOfOffer: number;
    Activities: Activities[];
}