import { DetailInInvoice } from './DetailsInInvoice';

export class Invoice{
    Id: number;
    IdOfSubscriptioners: number;
    NumberOfInvoice: string;
    StandardCostOfAllOffers: number;
    TotalCostOfExtraConnections: number;
    TotalCostOfExtraMessages: number;
    TotalCostForPeriod: number;
    Details: DetailInInvoice[];
}