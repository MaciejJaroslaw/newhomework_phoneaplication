export class DetailInInvoice{
    IdOfYourOffer: number;
    BaseCost: number;
    CostOfExtraSms: number;
    CostOfExtraConnections: number;
}