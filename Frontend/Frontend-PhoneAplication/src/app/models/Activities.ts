import { Timestamp } from 'rxjs';
import { SubscriptionersOffer } from './subscriptionersOffer';

export class Activities{
    Id: Number;
    PhoneNumber: string;
    KindOfAction: string;
    ConnectionTime: number;
    TimeOfRealization: Date;
    SubscribersOffer: SubscriptionersOffer;
}