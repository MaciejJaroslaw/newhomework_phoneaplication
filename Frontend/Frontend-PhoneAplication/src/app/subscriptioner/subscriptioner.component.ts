import { Component, OnInit } from '@angular/core';
import { SubscriptionerService } from '../Services/subscriptioner.service';
import { subscriptioner } from '../models/Subscriptioner';

@Component({
  selector: 'app-subsriptioner',
  templateUrl: './subscriptioner.component.html',
  styleUrls: ['./subscriptioner.component.css']
})
export class SubsriptionerComponent implements OnInit {
subscriptioners: subscriptioner[];

  constructor(private subscriptionerService: SubscriptionerService) { }

  ngOnInit() {
    this.getSubscriptioners()
}

    getSubscriptioners(): void{
      this.subscriptionerService.getallsubscriptioner().subscribe(
        subscriptioners => this.subscriptioners = subscriptioners);
    }

 
  // add(Id: number): void {
  //   Id = Id.valueOf();
  //   if (!Id) { return; }
  //   this.subscriptionerService.addsubscriptioner({ Id } as subscriptioner).subscribe(hero => {this.subscriptioners.push(subscriptioner);
  //     });
  // }
}