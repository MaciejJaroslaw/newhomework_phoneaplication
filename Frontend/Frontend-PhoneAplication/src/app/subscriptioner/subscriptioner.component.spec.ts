import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsriptionerComponent } from './subscriptioner.component';

describe('SubsriptionerComponent', () => {
  let component: SubsriptionerComponent;
  let fixture: ComponentFixture<SubsriptionerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubsriptionerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubsriptionerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
