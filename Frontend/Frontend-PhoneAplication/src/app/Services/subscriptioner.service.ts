import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { subscriptioner } from '../models/Subscriptioner';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'aplication/json'})}

@Injectable({
  providedIn: 'root'
})
export class SubscriptionerService {

  private subscriptionerApiURL = "http://localhost:1000/api/subscriptioners";

  constructor( private http: HttpClient) { }

  public getallsubscriptioner (): Observable<subscriptioner[]> {
    return this.http.get<subscriptioner[]>(this.subscriptionerApiURL);
  }
  
  public addsubscriptioner(offer: subscriptioner){
   return this.http.post<subscriptioner>(this.subscriptionerApiURL, offer, httpOptions)
   .subscribe(result => alert('New subscriptioner is added'))
  }
}
