import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SubscriptionersOffer } from '../models/subscriptionersOffer';
import { Invoice } from '../models/Invoice';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'aplication/json'})}

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  private invoiceApiURL = "http://localhost:1000/api/invoices";

  constructor( private http: HttpClient) { }

  public getalloffersforcustomer (id:number): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(this.invoiceApiURL);
  }

  public exportinvoicetofiles(invoiceNumber: string, formatOfFile: string, path: string){
    let url = `${this.invoiceApiURL}/${'exportinvoicetofiles'}`;
    return this.http.post<Invoice>(url, Message);
  }
  
  public addinvoice(invoice: Invoice){
   return this.http.post<Invoice>(this.invoiceApiURL, invoice, httpOptions)
   .subscribe(result => alert('New invoice is generated'))
  }

}
