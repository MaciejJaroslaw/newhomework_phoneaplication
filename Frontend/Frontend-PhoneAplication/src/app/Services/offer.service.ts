import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Offer } from '../models/Offer';
import { Observable } from '../../../node_modules/rxjs';
import { SubscriptionersOffer } from '../models/subscriptionersOffer';
import { $ } from '../../../node_modules/protractor';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'aplication/json'})}

@Injectable({
  providedIn: 'root'
})
export class OfferService {

  private offerApiURL = "http://localhost:1000/api/offer";

  constructor( private http: HttpClient) { }

  public getalloffers (): Observable<Offer[]> {
    return this.http.get<Offer[]>(this.offerApiURL);
  }

  public getOffer(id:number){
    let url = `${this.offerApiURL}/${id}`;
    return this.http.get<Offer>(url);
  }
  
  public addoffer(offer: Offer){
   return this.http.post<Offer>(this.offerApiURL, offer, httpOptions)
   .subscribe(result => alert('New Offer is added'))
  }


  public assiginOffer(offerId: number, subscriptionerId : number){
    return this.http.post<SubscriptionersOffer>(this.offerApiURL, SubscriptionersOffer, httpOptions).
    subscribe(result => alert('The number of contract is =${Id} and new number of phone for this subscriptioner is =${numberOfPhone}'))
  }
}

