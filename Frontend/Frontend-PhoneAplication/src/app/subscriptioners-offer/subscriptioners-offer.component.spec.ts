import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionersOfferComponent } from './subscriptioners-offer.component';

describe('SubscriptionersOfferComponent', () => {
  let component: SubscriptionersOfferComponent;
  let fixture: ComponentFixture<SubscriptionersOfferComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionersOfferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionersOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
