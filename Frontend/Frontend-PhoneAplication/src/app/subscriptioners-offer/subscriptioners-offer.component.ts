import { Component, OnInit } from '@angular/core';
import { SubscriptionersOffer } from '../models/subscriptionersOffer';
import { Offer } from '../models/Offer';
import { OfferService } from '../Services/offer.service';

@Component({
  selector: 'app-subscriptioners-offer',
  templateUrl: './subscriptioners-offer.component.html',
  styleUrls: ['./subscriptioners-offer.component.css']
})
export class SubscriptionersOfferComponent implements OnInit {
subscriptionersOffers: SubscriptionersOffer[];
offers: Offer[];
  constructor(private offerService: OfferService) { }

  ngOnInit() {
  this.getOffers();
}

getOffers(): void {
  this.offerService.getalloffers()
  .subscribe(offers => this.offers = offers);
}

getOffer(id: number): void{
  this.offerService.getOffer(id)
}


// addOffer(offer: Offer): void{
//   offer = name.trim();
//   if(!name) {return;}
//   this.offerService.addoffer({offer}) as Offer).subscribe(offer => {this.offers.push(offer);});
  
// }

// public assiginOffer(offerId: number, subscriptionerId : number){
//   return this.http.post<SubscriptionersOffer>(this.offerApiURL, SubscriptionersOffer, httpOptions).
//   subscribe(result => alert('The number of contract is =${Id} and new number of phone for this subscriptioner is =${numberOfPhone}'))
}