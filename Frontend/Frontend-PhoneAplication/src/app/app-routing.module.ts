import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubsriptionerComponent }      from './subscriptioner/subscriptioner.component';

const routes: Routes = [
  { path: 'subscriptioners', component: SubsriptionerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
