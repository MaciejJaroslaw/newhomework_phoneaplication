import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OfferComponent } from './offer/offer.component';
import { SubsriptionerComponent } from './subscriptioner/subscriptioner.component';
import { SubscriptionersOfferComponent } from './subscriptioners-offer/subscriptioners-offer.component';
import { InvoiceComponent } from './invoice/invoice.component';

@NgModule({
  declarations: [
    AppComponent,
    OfferComponent,
    SubsriptionerComponent,
    SubscriptionersOfferComponent,
    InvoiceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
