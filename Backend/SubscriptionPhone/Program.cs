﻿using HomeWorkPhoneAplication;
using HomeWorkPhoneAplication.BusinessLogic.Services;
using HomeWorkPhoneAplication.Helpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Configuration;
using Ninject;

namespace SubscriptionPhone
{
    class Program : IProgram
    { 
        
        private static string GetPhoneNumber()
        {
           return ConfigurationManager.AppSettings["phoneNumber"];
        }

        IOffersService _offersService;
        IPhonesService _phoneService;
        Stopwatch _stopWatch = new Stopwatch();
        private Menu _menu = new Menu();
        private bool quit;

        public Program(IOffersService offersService, IPhonesService phonesService)
        {
            _offersService = offersService;
            _phoneService = phonesService;
        }

        static void Main(string[] args)
        {
            IKernel nInjectContainer = new StandardKernel();
            nInjectContainer.Bind<IOffersService>().To<OffersService>();
            nInjectContainer.Bind<IPhonesService>().To<PhonesService>();

            nInjectContainer.Get<IProgram>().Run();
        }

        public void Run()
        {
            _menu.AddCommand("Exit", Exit);
            _menu.AddCommand("Send Message", SendMessage);
            _menu.AddCommand("Start calling", Calling);
            _menu.AddCommand("Show Current state of acount", ShowCurrentState);

            while (!quit)
            {
                _menu.PrintAllCommands();
                var command = IoHelper.GetStringFromUser("Select command");
                _menu.RunCommand(command);
            }
        }

        public void Exit()
        {
            quit = true;
        }

        public void SendMessage()
        {
            var phoneNumber = GetPhoneNumber();
            Console.WriteLine("Enter your message here");
            Console.ReadLine();

            _phoneService.SendMessage(phoneNumber);
            _offersService.OverwriteStateOfSmsInOffer(phoneNumber);

            if (_phoneService.ComparisonStateOfSmsInOffer(phoneNumber))
            {
                var costOfSms = _offersService.GetCostOfMessage(phoneNumber);
                var totalCost = _phoneService.TotalCostOfMessage(phoneNumber);

                Console.WriteLine($"Cost of this sms is {costOfSms} pln");
                Console.WriteLine($"Cost of all your message over the package is {totalCost} pln");
            }
            else
                Console.WriteLine("Your message was realized in package of free sms");
        }

        public void Calling()
        {
            var phoneNumber = GetPhoneNumber();
            Console.WriteLine("Put the enter to start calling");
            Console.ReadLine();
            _stopWatch.Start();
            Console.WriteLine("Put the enter to stop calling");
            Console.ReadLine();
            _stopWatch.Stop();
            TimeSpan talkTime = _stopWatch.Elapsed;
            Console.WriteLine($"Time of your connection is {talkTime}");

            _phoneService.VoiceConnection(talkTime, phoneNumber);
            _offersService.OverwriteStateOfOffer(phoneNumber, talkTime);

            if (_phoneService.ComparisonStateOfOffer(phoneNumber))
            {
                var costOfConnection = _phoneService.CalculateTheCost(phoneNumber);

                Console.WriteLine($"Cost of your connections over the package is {costOfConnection}");
            }
            else
                Console.WriteLine("Your connection was realized in package of free minutes");
        }

        public void ShowCurrentState()
        {
            var phoneNumber = GetPhoneNumber();

            var id = _offersService.GetSubscriptionersOfferId(phoneNumber);
            var offerBl = _offersService.GetOffer(id);
            var costForCalling = _phoneService.CalculateTheCost(phoneNumber);
            var totalCost = _phoneService.TotalCostOfMessage(phoneNumber);

            Console.WriteLine($"For your offert {offerBl.Name} standard cost for one months is {offerBl.CostOfOnePeriod}pln");
            Console.WriteLine("Your costs over the package are:");

            if(_phoneService.ComparisonStateOfOffer(phoneNumber))
                Console.WriteLine($"For voice connects: {costForCalling} pln");
            else
                Console.WriteLine("For voice connects: 0 pln");


            if (_phoneService.ComparisonStateOfSmsInOffer(phoneNumber))
                Console.WriteLine($"For all your message over the package is {totalCost} pln");
            else
                Console.WriteLine("For all your message over the package is 0 pln");
        }
    }
}