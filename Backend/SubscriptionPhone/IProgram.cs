﻿namespace SubscriptionPhone
{
    interface IProgram
    {
        void Run();
        void Calling();
        void Exit();
        void SendMessage();
        void ShowCurrentState();
    }
}