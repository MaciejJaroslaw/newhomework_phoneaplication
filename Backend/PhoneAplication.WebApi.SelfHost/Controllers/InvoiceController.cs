﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.BusinessLogic.Services;
using HomeWorkPhoneAplication.Helpers;
using HomeWorkPhoneAplication.Tools.FileSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace WebApiForPhoneAplication.Controllers
{
    [RoutePrefix("api/invoices")]
    public class InvoiceController : ApiController
    {
        IChangerFilesFormat _changerFilesFortmat;
        ISubscriptionersService _subscriptionersService;
        ISubscriptionerController _subscriptionerController;
        IInvoicesService _invoiceService;
        public InvoiceController(IInvoicesService invoicesService, ISubscriptionerController subscriptionerController,
             ISubscriptionersService subscriptionersService, IChangerFilesFormat changerFilesFortmat)
        {
            _changerFilesFortmat = changerFilesFortmat;
            _invoiceService = invoicesService;
            _subscriptionerController = subscriptionerController;
            _subscriptionersService = subscriptionersService;
        }

        [HttpPost]
        [Route("addinvoice")]
        public InvoiceBl GenerateInvoice([FromUri]int subscriptionerId)
        {
            var invoiceBl = _invoiceService.GenerateInvoice(subscriptionerId);
            var invoice = _invoiceService.AddInvoice(invoiceBl);
            _subscriptionersService.StartAnewBillingPeriod(subscriptionerId);

            return invoice;
        }

        [HttpGet]
        [Route("getalloffersforcustomer")]
        public List<InvoiceBl> GetAllOfferForOneCustomer([FromUri] int Id)
        {
            return _invoiceService.GetListOfSubscriptionersInvoices(Id);
        }

        [HttpPost]
        [Route("exportinvoicetofiles")]
        public IHttpActionResult ExportInvoiceToFile([FromUri]string invoiceNumber, [FromUri] string formatOfFile, [FromUri] string path)
        {
            var invoiceToExport = _invoiceService.GetSelectedInvoice(invoiceNumber);
;
            var fileFormat = _changerFilesFortmat.GetSerializer(formatOfFile);
            var fileFormatName = fileFormat.GetFileFormat();
            _invoiceService.ExportToFile(path, invoiceToExport, fileFormatName);

            return Ok($"Invoice with Id {invoiceToExport.Id} has number {invoiceToExport.NumberOfInvoice} include theese costs:" +
                $"Standard cost for all your offer: {invoiceToExport.StandardCostOfAllOffers} pln, total cost for all extra connections: " +
                $"{invoiceToExport.TotalCostOfExtraConnections} pln and total cost for all extra messages: {invoiceToExport.TotalCostOfExtraMessages} pln" +
                $"together its give {invoiceToExport.TotalCostForPeriod} plm");
        }
    }
}