﻿using System.Collections.Generic;
using System.Web.Http;
using HomeWorkPhoneAplication.BusinessLogic.Models;

namespace WebApiForPhoneAplication.Controllers
{
    public interface ISubscriptionerController
    {
        IHttpActionResult CreateNewSubscriptioner([FromBody] SubscriptionerBl subscriptioner);
        List<SubscriptionerBl> GetallSubscriptioner();
    }
}