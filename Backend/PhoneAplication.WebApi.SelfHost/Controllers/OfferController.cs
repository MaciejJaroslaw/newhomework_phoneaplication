﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.BusinessLogic.Services;
using HomeWorkPhoneAplication.Helpers;
using System;
using System.Collections.Generic;
using System.Web.Http;


namespace WebApiForPhoneAplication.Controllers
{
    [RoutePrefix("api/offer")]

    public class OfferController : ApiController
    {
        ISubscriptionersService _subscriptionersService;
        IOffersService _offersService;
        ISubscriptionerController _subscriptionerController;
        public OfferController ( IOffersService offersService, ISubscriptionerController subscriptionerController, 
            ISubscriptionersService subscriptionersService)
        {
            _offersService = offersService;
            _subscriptionerController = subscriptionerController;
            _subscriptionersService = subscriptionersService; 
            _subscriptionersService = subscriptionersService;
        }

        [HttpPost]
        [Route("addoffer")]
        public IHttpActionResult CreateNewOffer([FromBody] OfferBl offerBl)
        {
            int Id = _offersService.AddOffer(offerBl);

            return Ok ($"New Offer has Id {Id}");
        }

        [HttpGet]
        [Route("getallofers")]
        public List<OfferBl> GetAllOffer()
        {
            return _offersService.GetAll();
        }

        [HttpPost]
        [Route("assignoffer")]
        public IHttpActionResult AssignOfferToCustomer([FromUri]int subscriptionerId, int offerId)
        {         
            var offer = _offersService.GetOffer(offerId);
            var numberOfPhone = _subscriptionersService.SubscribeNewOffer(subscriptionerId, offerId, offer.CostOfOnePeriod);
            var Id = _subscriptionersService.GetIdOfLastAssignedOffer(); 

            return Ok ($"The number of contract is {Id} and new number of phone for this subscriptioner is {numberOfPhone}");
        }
    }
}