﻿using System;
using Microsoft.Owin.Hosting;
using Topshelf;
using System.Configuration;

namespace PhoneAplication.WebApi.SelfHost
{
    class Program
    {
        static void Main(string[] args)
        {
            var rc = HostFactory.Run(x =>
            {
                x.Service<Application>(s =>
                {
                    s.ConstructUsing(name => new Application());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("API for PhoneAplication");
                x.SetDisplayName("Phone Service Api");
                x.SetServiceName("PhoneServiceApi");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetType());
            Environment.ExitCode = exitCode;
        }

        internal class Application
        {
            private IDisposable _api;

            public void Start()
            {
                _api = WebApp.Start<StartUp>(ConfigurationManager.AppSettings["url"]);
            }

            public void Stop()
            {
                _api.Dispose();
            }
        }
    }
}
