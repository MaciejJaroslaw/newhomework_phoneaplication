﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.BusinessLogic.Services;
using HomeWorkPhoneAplication.Helpers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiForPhoneAplication.Controllers
{
    [RoutePrefix("api/subscriptioners")]
    public class SubscriptionerController : ApiController, ISubscriptionerController
    {
        ISubscriptionersService _subscriptionersService;
        public SubscriptionerController(ISubscriptionersService subscriptionersService)
        {
            _subscriptionersService = subscriptionersService;
        }

        [HttpPost]
        [Route("addsubscriptioner")]
        public IHttpActionResult CreateNewSubscriptioner([FromBody] SubscriptionerBl subscriptioner)
        {
            var correctness = _subscriptionersService.CheckTheEmail(subscriptioner.Email);
            if (correctness == true)
            {
                var Id = _subscriptionersService.AddSubscriptioner(subscriptioner);
                var BillingPeriod = subscriptioner.BillingPeriod;

                return Ok($"Your profile is added to database, your Id is {Id}." +
                       $"Your billing period is {BillingPeriod}th of every mont");
            }
            else
                return BadRequest();
        }

        [HttpGet]
        [Route("getallsubscriptioner")]
        public List<SubscriptionerBl> GetallSubscriptioner()
        {
            return _subscriptionersService.GetAll();
        }
    }
}