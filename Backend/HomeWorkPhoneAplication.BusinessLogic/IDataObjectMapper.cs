﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.DataLogic.Models;

namespace HomeWorkPhoneAplication.BusinessLogic
{
    public interface IDataObjectMapper
    {
        Activities MapActicitiesBlToActivities(ActivitiesBl activitiesBl);
        ActivitiesBl MapActivitiesToActivitiesBl(Activities activities);
        DetailsInInvoice MapDetailsInInvoiceBlToDetailsInInvoice(DetailsInInvoiceBl detailsBl);
        DetailsInInvoiceBl MapDetailsInInvoiceToDetailsInInvoiceBl(DetailsInInvoice details);
        Invoice MapInvoiceBlToInvoice(InvoiceBl invoiceBl);
        InvoiceBl MapInvoiceToInvoiceBl(Invoice invoice);
        Offer MapOfferBlToOffer(OfferBl offerBl);
        OfferBl MapOfferToOfferBl(Offer offer);
        SubscriptionerBl MapSubcriptionerToSubscriptionerBl(Subscriptioner subscriptioner);
        Subscriptioner MapSubscriptionerBlToSubscriptioner(SubscriptionerBl subscriptionerBl);
        SubscriptionersOffer MapSubscriptionersOfferBlToSubscriptionerOffer(SubscriptionersOfferBl subscriptionersOfferBl);
        SubscriptionersOfferBl MapSubscriptionersOfferToSubscriptionersOfferBl(SubscriptionersOffer subscriptionersOffer);
    }
}