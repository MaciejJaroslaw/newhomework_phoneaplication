﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Models
{
    public class OfferBl
    {
        public int Id;
        public string Name;
        public int PackageOfFreeMinutes;
        public int PackageOfFreeSms;
        public decimal CostOfMinute;
        public decimal CostOfSms;
        public decimal CostOfOnePeriod;
    }
}