﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Models
{
    public class InvoiceBl
    {
        public int Id;
        public int IdOfSubscriptioners;
        public string NumberOfInvoice;
        public decimal StandardCostOfAllOffers;
        public decimal TotalCostOfExtraConnections;
        public decimal TotalCostOfExtraMessages;
        public decimal TotalCostForPeriod;

        public List<DetailsInInvoiceBl> details = new List<DetailsInInvoiceBl>();
    }
}
