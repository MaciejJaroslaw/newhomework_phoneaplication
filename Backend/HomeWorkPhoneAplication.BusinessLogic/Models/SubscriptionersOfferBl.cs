﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Models
{
    public class SubscriptionersOfferBl
    {
        public int Id;
        public int IdOfOffer;
        public int IdOfSubscriptioner;
        public string NumberOfPhone;
        public int TimeOfAllConnections;
        public int AmmountOfAllMessage;
        public decimal CostOfOffer;

        public List<ActivitiesBl> Activities = new List<ActivitiesBl>();
    }
}
