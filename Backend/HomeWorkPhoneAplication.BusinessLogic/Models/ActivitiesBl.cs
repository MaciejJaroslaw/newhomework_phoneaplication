﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Models
{
    public class ActivitiesBl
    {
        public int Id;
        public string PhoneNumber;
        public string KindOfAction;
        public TimeSpan ConnectionTime;
        public DateTime TimeOfRealization;
        public SubscriptionersOfferBl SubscribersOffer;
    }
}
