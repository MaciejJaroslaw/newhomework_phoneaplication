﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Models
{
    public class SubscriptionerBl
    {
        public int Id;
        public string Name;
        public string Surname;
        public string Email;
        public int BillingPeriod;
        public DateTime DateOfBorn;
    }
}
