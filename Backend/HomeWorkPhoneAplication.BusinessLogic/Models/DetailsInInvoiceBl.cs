﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Models
{
    public class DetailsInInvoiceBl
    {
        public int IdOfYourOffer;
        public decimal BaseCost;
        public decimal CostOfExtraSms;
        public decimal CostOfExtraConnections;
    }
}
