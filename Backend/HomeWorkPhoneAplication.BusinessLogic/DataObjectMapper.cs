﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic
{
    public class DataObjectMapper : IDataObjectMapper
    {
        public SubscriptionerBl MapSubcriptionerToSubscriptionerBl (Subscriptioner subscriptioner)
        {
            var subscriptionerBl = new SubscriptionerBl
            {
                Name = subscriptioner.Name,
                Surname = subscriptioner.Surname,
                BillingPeriod = subscriptioner.BillingPeriod,
                DateOfBorn = subscriptioner.DateOfBorn,
                Email = subscriptioner.Email,
                Id = subscriptioner.Id,
            };
            return subscriptionerBl;
        }

        public Subscriptioner MapSubscriptionerBlToSubscriptioner (SubscriptionerBl subscriptionerBl)
        {
            var subscriptioner = new Subscriptioner
            {
                Name = subscriptionerBl.Name,
                Surname = subscriptionerBl.Surname,
                BillingPeriod = subscriptionerBl.BillingPeriod,
                DateOfBorn = subscriptionerBl.DateOfBorn,
                Email = subscriptionerBl.Email,
                Id = subscriptionerBl.Id,    
            };
            return subscriptioner;
        }

        public OfferBl MapOfferToOfferBl (Offer offer)
        {
            var offerBl = new OfferBl
            {
                Name = offer.Name,
                PackageOfFreeMinutes = offer.PackageOfFreeMinutes,
                PackageOfFreeSms = offer.PackageOfFreeSms,
                CostOfMinute = offer.CostOfMinute,
                CostOfSms = offer.CostOfSms,
                CostOfOnePeriod = offer.CostOfOnePeriod,
                Id = offer.Id,
            };
            return offerBl;
        }

        public Offer MapOfferBlToOffer (OfferBl offerBl)
        {
            var offer = new Offer
            {
                Name = offerBl.Name,
                PackageOfFreeMinutes = offerBl.PackageOfFreeMinutes,
                PackageOfFreeSms = offerBl.PackageOfFreeSms,
                CostOfMinute = offerBl.CostOfMinute,
                CostOfSms = offerBl.CostOfSms,
                CostOfOnePeriod = offerBl.CostOfOnePeriod,
                Id = offerBl.Id,
            };
            return offer;
        }

        public ActivitiesBl MapActivitiesToActivitiesBl(Activities activities)
        {
            var activitiesBl = new ActivitiesBl
            {
                PhoneNumber = activities.PhoneNumber,
                KindOfAction = activities.KindOfAction,
                TimeOfRealization = activities.TimeOfRealization,
                ConnectionTime = activities.ConnectionTime,
                Id = activities.Id
            };
            return activitiesBl;
        }

        public Activities MapActicitiesBlToActivities(ActivitiesBl activitiesBl)
        {
            var activities = new Activities
            {
                PhoneNumber = activitiesBl.PhoneNumber,
                KindOfAction = activitiesBl.KindOfAction,
                TimeOfRealization = activitiesBl.TimeOfRealization,
                ConnectionTime = activitiesBl.ConnectionTime,
                Id = activitiesBl.Id
            };
            return activities;
        }

        public SubscriptionersOfferBl MapSubscriptionersOfferToSubscriptionersOfferBl(SubscriptionersOffer subscriptionersOffer)
        {
            var subscriptionersOfferBl = new SubscriptionersOfferBl
            {
                Id = subscriptionersOffer.Id,
                IdOfOffer = subscriptionersOffer.IdOfOffer,
                IdOfSubscriptioner = subscriptionersOffer.IdOfSubscriptioner,
                NumberOfPhone = subscriptionersOffer.NumberOfPhone,
                TimeOfAllConnections = subscriptionersOffer.TimeOfAllConnections,
                AmmountOfAllMessage = subscriptionersOffer.AmmountOfAllMessage,
                CostOfOffer = subscriptionersOffer.CostOfOffer
            };
            return subscriptionersOfferBl;
        }

        public SubscriptionersOffer MapSubscriptionersOfferBlToSubscriptionerOffer(SubscriptionersOfferBl subscriptionersOfferBl)
        {
            var subscriptionersOffer = new SubscriptionersOffer
            {
                Id = subscriptionersOfferBl.Id,
                IdOfOffer = subscriptionersOfferBl.IdOfOffer,
                IdOfSubscriptioner = subscriptionersOfferBl.IdOfSubscriptioner,
                NumberOfPhone = subscriptionersOfferBl.NumberOfPhone,
                TimeOfAllConnections = subscriptionersOfferBl.TimeOfAllConnections,
                AmmountOfAllMessage = subscriptionersOfferBl.AmmountOfAllMessage,
                CostOfOffer = subscriptionersOfferBl.CostOfOffer
            };
            return subscriptionersOffer;
        }

        public InvoiceBl MapInvoiceToInvoiceBl(Invoice invoice)
        {
            var invoiceBl = new InvoiceBl
            {
                Id = invoice.Id,
                IdOfSubscriptioners = invoice.IdOfSubscriptioners,
                StandardCostOfAllOffers = invoice.StandardCostOfAllOffers,
                NumberOfInvoice = invoice.NumberOfInvoice,
                TotalCostForPeriod = invoice.TotalCostForPeriod,
                TotalCostOfExtraConnections = invoice.TotalCostOfExtraConnections,
                TotalCostOfExtraMessages = invoice.TotalCostOfExtraMessages,
                details = invoice.details.Select(MapDetailsInInvoiceToDetailsInInvoiceBl).ToList()              
            };
            return invoiceBl;
        }

        public Invoice MapInvoiceBlToInvoice(InvoiceBl invoiceBl)
        {
            var invoice = new Invoice
            {
                Id = invoiceBl.Id,
                IdOfSubscriptioners = invoiceBl.IdOfSubscriptioners,
                StandardCostOfAllOffers = invoiceBl.StandardCostOfAllOffers,
                NumberOfInvoice = invoiceBl.NumberOfInvoice,
                TotalCostForPeriod = invoiceBl.TotalCostForPeriod,
                TotalCostOfExtraConnections = invoiceBl.TotalCostOfExtraConnections,
                TotalCostOfExtraMessages = invoiceBl.TotalCostOfExtraMessages,
                details = invoiceBl.details.Select(MapDetailsInInvoiceBlToDetailsInInvoice).ToList()
            };
            return invoice;
        }

        public DetailsInInvoiceBl MapDetailsInInvoiceToDetailsInInvoiceBl(DetailsInInvoice details)
        {
            var detailsBl = new DetailsInInvoiceBl
            {
                IdOfYourOffer = details.IdOfYourOffer,
                BaseCost = details.BaseCost,
                CostOfExtraSms = details.CostOfExtraSms,
                CostOfExtraConnections = details.CostOfExtraConnections
            };
            return detailsBl;
        }
        public DetailsInInvoice MapDetailsInInvoiceBlToDetailsInInvoice(DetailsInInvoiceBl detailsBl)
        {
            var details = new DetailsInInvoice
            {
                IdOfYourOffer = detailsBl.IdOfYourOffer,
                CostOfExtraSms = detailsBl.CostOfExtraSms,
                BaseCost = detailsBl.BaseCost,
                CostOfExtraConnections = detailsBl.CostOfExtraConnections
            };
            return details;
        }

    }
}