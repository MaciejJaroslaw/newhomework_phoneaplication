﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using HomeWorkPhoneAplication.DataLogic;
using HomeWorkPhoneAplication.DataLogic.Models;
using System.Data;
using System.Text.RegularExpressions;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public class SubscriptionersService : ISubscriptionersService
    {
        IDataObjectMapper _dataObjectsMapper = new DataObjectMapper();
        IOffersService _offersService = new OffersService();
        private Random _generator = new Random();

        public int AddSubscriptioner (SubscriptionerBl subscriptionerBl)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var subscriptioner = dbContext.SubscriptionersDbSet.Add(_dataObjectsMapper.MapSubscriptionerBlToSubscriptioner(subscriptionerBl));
                dbContext.SaveChanges();
                return subscriptioner.Id;
            }
        }

        public bool CheckTheEmail(string email)
        {
            var regex = new Regex(@"(?<username>[a-z][A-z0-9._-]*)@(?<domain>[A-z0-9._]+)\.(?<country>[A-z]{2,3})");

            var result = regex.Match(email);
            if (!result.Success)
                return false;
            else
                return true;
        }

        public int GenerateBillingPeriod()
        {
            int billingPeriod;
            int termOfPeriod1;
            int termOfPeriod10;
            int termOfPeriod20;

            var _termOfPeriod1 = 1;
            var _termOfPeriod10 = 10;
            var _termOfPeriod20 = 20;


            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                termOfPeriod1 = dbContext.SubscriptionersDbSet.Where(x => x.BillingPeriod == _termOfPeriod1).Count();
                termOfPeriod10 = dbContext.SubscriptionersDbSet.Where(x => x.BillingPeriod == _termOfPeriod10).Count();
                termOfPeriod20 = dbContext.SubscriptionersDbSet.Where(x => x.BillingPeriod == _termOfPeriod20).Count();
            }

            billingPeriod = termOfPeriod1 <= termOfPeriod10 && termOfPeriod1 <= termOfPeriod20  
                ? _termOfPeriod1 : termOfPeriod10 < termOfPeriod1 && termOfPeriod10 <= termOfPeriod20 
                ? _termOfPeriod10 : _termOfPeriod20;

            return billingPeriod;
        }

        public List<SubscriptionerBl> GetAll()
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var list = dbContext.SubscriptionersDbSet;
                var listBl = new List<SubscriptionerBl>();

                foreach (Subscriptioner subscriptioner in list)
                {
                    listBl.Add(_dataObjectsMapper.MapSubcriptionerToSubscriptionerBl(subscriptioner));
                }
                return listBl;
            }
        }

        public string SubscribeNewOffer(int subscriptionerId, int offerId, decimal cost)
        {
            var subscriptionersOfferBl = new SubscriptionersOfferBl
            {
                IdOfOffer = offerId,
                IdOfSubscriptioner = subscriptionerId,
                CostOfOffer = cost
            };

            var NumberOfPhone = _offersService.CreateNewNumber();
            subscriptionersOfferBl.NumberOfPhone = NumberOfPhone;

            SubscriptionersOffer subscriptionersOffer = 
                _dataObjectsMapper.MapSubscriptionersOfferBlToSubscriptionerOffer(subscriptionersOfferBl);

            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                dbContext.SubscriptionersOffersDbSet.Add(subscriptionersOffer);
                dbContext.SaveChanges();
            }
            return NumberOfPhone;
        }

        public int GetIdOfLastAssignedOffer()
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
              return  dbContext.SubscriptionersOffersDbSet.Max(x => x.Id);
            }
        }

        public Subscriptioner GetSubscriptioner(int Id)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                return dbContext.SubscriptionersDbSet.SingleOrDefault(x => x.Id == Id);
            }
        }

        public int GetSubscriptionerId(string phoneNumber)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
               var subscriptionersOffer = dbContext.SubscriptionersOffersDbSet.SingleOrDefault(y => y.NumberOfPhone == phoneNumber);
               int Id = subscriptionersOffer.IdOfSubscriptioner;

                return Id;
            }
        }

        public List<SubscriptionersOfferBl> GetAllSubscriptionersOffer(int Id)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
               List<SubscriptionersOffer> Offers = dbContext.SubscriptionersOffersDbSet.Where(x => x.IdOfSubscriptioner == Id).ToList();
                List<SubscriptionersOfferBl> OffersBl = new List<SubscriptionersOfferBl>();
            
                foreach(SubscriptionersOffer offer in Offers)
                {
                   OffersBl.Add(_dataObjectsMapper.MapSubscriptionersOfferToSubscriptionersOfferBl(offer));
                }

                return OffersBl;
            }
        }

        public void StartAnewBillingPeriod(int SubscriptionerId)
        {
            List<SubscriptionersOfferBl> offers = GetAllSubscriptionersOffer(SubscriptionerId);
            foreach (var offer in offers)
            {
                offer.TimeOfAllConnections = 0;
                offer.AmmountOfAllMessage = 0;

                _offersService.UpdateSubscriptionersOffer(_dataObjectsMapper.MapSubscriptionersOfferBlToSubscriptionerOffer(offer));
            }
        }

        public void UpdateSubscriptioner(Subscriptioner subscriptioner)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                dbContext.SubscriptionersDbSet.Attach(subscriptioner);
                dbContext.Entry(subscriptioner).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }
    }
}