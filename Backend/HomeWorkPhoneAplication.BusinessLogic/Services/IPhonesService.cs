﻿using System;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public interface IPhonesService
    {
        string Sms { get; set; }
        string VoiceCall { get; set; }

        decimal CalculateTheCost(string phoneNumber);
        bool ComparisonStateOfOffer(string phoneNumber);
        bool ComparisonStateOfSmsInOffer(string phoneNumber);
        int GetTotalTalkTimeOverPackage(string phoneNumber);
        void SendMessage(string phoneNumber);
        decimal TotalCostOfMessage(string phoneNumber);
        void VoiceConnection(TimeSpan talkTime, string phoneNumber);
    }
}