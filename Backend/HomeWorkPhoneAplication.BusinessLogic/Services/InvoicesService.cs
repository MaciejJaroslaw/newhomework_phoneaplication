﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.DataLogic;
using HomeWorkPhoneAplication.DataLogic.Models;
using HomeWorkPhoneAplication.Tools.FileSerializer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public class InvoicesService : IInvoicesService
    {
        SubscriptionersService _subscriptionersService = new SubscriptionersService();
        PhonesService _phonesService = new PhonesService();
        OffersService _offersService = new OffersService();
        IDataObjectMapper _dataObjectMapper = new DataObjectMapper();
        public ISerializator _serializer = new XMLFileManager();
        public ISerializator _serializator = new JsonFileManager();

        public InvoiceBl GenerateInvoice(int subscriptionerId)
        {
            var offers = _subscriptionersService.GetAllSubscriptionersOffer(subscriptionerId);


            InvoiceBl invoiceBl = new InvoiceBl();
            invoiceBl.NumberOfInvoice = GenerateNumberOfInvoice(subscriptionerId);
            invoiceBl.IdOfSubscriptioners = subscriptionerId;
            invoiceBl.StandardCostOfAllOffers = CalculateBaseCostForOffers(subscriptionerId);
            invoiceBl.TotalCostOfExtraConnections = CalculateExtraConnectionsCostForOffers(subscriptionerId);
            invoiceBl.TotalCostOfExtraMessages = CalculateExtraSmsCostForOffers(subscriptionerId);
            invoiceBl.TotalCostForPeriod = TotalCostForPeriod(invoiceBl.StandardCostOfAllOffers,
                invoiceBl.TotalCostOfExtraConnections, invoiceBl.TotalCostOfExtraMessages);

                foreach (SubscriptionersOfferBl SubscriptionersOffer in offers)
                {
                    DetailsInInvoiceBl detailes = new DetailsInInvoiceBl
                    {
                        IdOfYourOffer = SubscriptionersOffer.Id,
                        BaseCost = SubscriptionersOffer.CostOfOffer,
                        CostOfExtraConnections = _phonesService.CalculateTheCost(SubscriptionersOffer.NumberOfPhone),
                        CostOfExtraSms = _phonesService.TotalCostOfMessage(SubscriptionersOffer.NumberOfPhone)
                    };

                    invoiceBl.details.Add(detailes);
                }
            
            return invoiceBl;
        }

        public InvoiceBl AddInvoice(InvoiceBl invoiceBl)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var invoice = dbContext.InvoicesDbSet.Add(_dataObjectMapper.MapInvoiceBlToInvoice(invoiceBl));
                dbContext.SaveChanges();

                return _dataObjectMapper.MapInvoiceToInvoiceBl(invoice);
            }
        }

        public string GenerateNumberOfInvoice(int subscriptionerId)
        {
            var subscriptionerUid = subscriptionerId;
            var invoiceId = GetIdOfLastInvoiceSelectedSubscriptioners(subscriptionerId);
            var invoiceNumber = $"FV_{subscriptionerUid:D4}{invoiceId +1:D4}";

            return invoiceNumber;
        }

        public int GetIdOfLastInvoiceSelectedSubscriptioners(int id)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                if ((dbContext.InvoicesDbSet.Where(x => x.IdOfSubscriptioners == id).Any()))
                {
                    return dbContext.InvoicesDbSet.Where(x => x.IdOfSubscriptioners == id).Max(x => x.Id);
                }
                else
                    return 0;
            }
        }

        public decimal CalculateBaseCostForOffers(int Id)
        {
            decimal value = 0;
            var SubscriptionersOffers = _subscriptionersService.GetAllSubscriptionersOffer(Id);

            foreach (var SubscriptionersOffer in SubscriptionersOffers)
            {
                var offerBl = _offersService.GetOffer(SubscriptionersOffer.IdOfOffer);
                value += offerBl.CostOfOnePeriod;
            }

            return value;
        }

        public decimal CalculateExtraSmsCostForOffers(int Id)
        {
            decimal value = 0;
            var Offers = _subscriptionersService.GetAllSubscriptionersOffer(Id);

            foreach (var offer in Offers)
            {
                value += _phonesService.TotalCostOfMessage(offer.NumberOfPhone);
            }

            return value;
        }

        public decimal CalculateExtraConnectionsCostForOffers(int Id)
        {
            decimal value = 0;
            var Offers = _subscriptionersService.GetAllSubscriptionersOffer(Id);

            foreach (var offer in Offers)
            {
                value += _phonesService.CalculateTheCost(offer.NumberOfPhone);
            }

            return value;
        }

        public decimal TotalCostForPeriod(decimal basicCost, decimal extraConectionsCost, decimal extraSmsCost)
        {
            return basicCost + extraConectionsCost + extraSmsCost;
        }

        public List<InvoiceBl> GetListOfSubscriptionersInvoices(int Id)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                List<Invoice> invoices = dbContext.InvoicesDbSet.Where(x => x.IdOfSubscriptioners == Id).ToList();
                List<InvoiceBl> invoicesBl = new List<InvoiceBl>();

                foreach (Invoice invoice in invoices)
                {
                    invoicesBl.Add(_dataObjectMapper.MapInvoiceToInvoiceBl(invoice));
                }

                return invoicesBl;
            }
        }

        public InvoiceBl GetSelectedInvoice(string numberOfInvoice)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var invoice = dbContext.InvoicesDbSet.SingleOrDefault(x => x.NumberOfInvoice == numberOfInvoice);
                InvoiceBl invoiceBl = _dataObjectMapper.MapInvoiceToInvoiceBl(invoice);

                return invoiceBl;
            }
        }

        public void ImportFromFile()
        {
            Console.WriteLine("Enter the file name:");
            var fileName = Console.ReadLine();

            var invoice = _serializer.ReadFromFile(fileName);
        }

        public void ExportToFile(string path, InvoiceBl invoiceBl, string fileFormat)
        {
            var invoice = _dataObjectMapper.MapInvoiceBlToInvoice(invoiceBl);

            var fileName = GenerateNameForInvoiceFile(invoice.NumberOfInvoice, fileFormat);

            if (fileFormat == "json")
            {
                _serializator.SaveToFile(path, fileName, invoice);
            }
            else
                _serializer.SaveToFile(path, fileName, invoice);
        }

        public string GenerateNameForInvoiceFile(string numberOfInvoice, string fileFormat)
        {
            var filesName = numberOfInvoice + "." + fileFormat;

            return filesName;
        }
    }
}
