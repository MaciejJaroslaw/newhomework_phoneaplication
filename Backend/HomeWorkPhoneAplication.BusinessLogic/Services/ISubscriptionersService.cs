﻿using System.Collections.Generic;
using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.DataLogic.Models;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public interface ISubscriptionersService
    {
        int AddSubscriptioner(SubscriptionerBl subscriptionerBl);
        bool CheckTheEmail(string email);
        int GenerateBillingPeriod();
        List<SubscriptionerBl> GetAll();
        List<SubscriptionersOfferBl> GetAllSubscriptionersOffer(int Id);
        Subscriptioner GetSubscriptioner(int Id);
        int GetSubscriptionerId(string phoneNumber);
        string SubscribeNewOffer(int subscriptionerId, int offerId, decimal cost);
        int GetIdOfLastAssignedOffer();
        void UpdateSubscriptioner(Subscriptioner subscriptioner);
        void StartAnewBillingPeriod(int SubscriptionerId);
    }
}