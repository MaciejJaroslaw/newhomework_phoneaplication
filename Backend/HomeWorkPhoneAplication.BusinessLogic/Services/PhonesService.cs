﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.DataLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public class PhonesService : IPhonesService
    {
        SubscriptionersService _subscriptionersService = new SubscriptionersService();
        OffersService _offersService = new OffersService();
        IDataObjectMapper _dataObjectMapper = new DataObjectMapper();

        public string Sms { get; set; }
        public string VoiceCall { get; set; }

        public void SendMessage(string phoneNumber)
        {
            var activitiesBl = new ActivitiesBl
            {
                PhoneNumber = phoneNumber,
                KindOfAction = Sms,
                TimeOfRealization = DateTime.Now,
            };

            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var subscriptionersOffer = dbContext.SubscriptionersOffersDbSet.SingleOrDefault(y => y.NumberOfPhone == phoneNumber);
                var activity = _dataObjectMapper.MapActicitiesBlToActivities(activitiesBl);

                activity.SubscribersOffer = subscriptionersOffer;

                dbContext.ActivitiesDbSet.Add(activity);
                dbContext.SaveChanges();
            }
        }

        public void VoiceConnection(TimeSpan talkTime, string phoneNumber)
        {
            var activitiesBl = new ActivitiesBl
            {
                PhoneNumber = phoneNumber,
                KindOfAction = VoiceCall,
                ConnectionTime = talkTime,
                TimeOfRealization = DateTime.Now,
                SubscribersOffer = _offersService.GetSubscriptionersOffer(phoneNumber)
            };

            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var subscriptionersOffer = dbContext.SubscriptionersOffersDbSet.SingleOrDefault(y => y.NumberOfPhone == phoneNumber);
                var activity = _dataObjectMapper.MapActicitiesBlToActivities(activitiesBl);

                activity.SubscribersOffer = subscriptionersOffer;

                dbContext.ActivitiesDbSet.Add(activity);
                dbContext.SaveChanges();
            }
        }

        public decimal CalculateTheCost(string phoneNumber)
        {
            int Id = _offersService.GetSubscriptionersOfferId(phoneNumber);
            decimal cost = _offersService.GetCostOfConnection(Id);
            var totalTime = GetTotalTalkTimeOverPackage(phoneNumber);

            return (cost / 60) * totalTime;
        }

        public decimal TotalCostOfMessage(string phoneNumber)
        {
            int Id = _offersService.GetSubscriptionersOfferId(phoneNumber);
            var offer = _offersService.GetOffer(Id);
            var costOfSms = offer.CostOfSms;

            var subscriptionersOffer = _offersService.GetSubscriptionersOffer(phoneNumber);

            var amountOfSms = (subscriptionersOffer.AmmountOfAllMessage - offer.PackageOfFreeSms);

            if (amountOfSms > 0)
            {
                return amountOfSms * costOfSms;
            }
            else
                return 0 * costOfSms;
        }

        public int GetTotalTalkTimeOverPackage(string phoneNumber)
        {
            var id = _offersService.GetSubscriptionersOfferId(phoneNumber);
            var offer = _offersService.GetOffer(id);
            var subscriptionersOffer = _offersService.GetSubscriptionersOffer(phoneNumber);

            
            int totalTime = subscriptionersOffer.TimeOfAllConnections - offer.PackageOfFreeMinutes * 60;

            if (totalTime > 0)
                return totalTime;
            else
                return 0;
        }
            
        public bool ComparisonStateOfOffer(string phoneNumber)
        {
            var id = _offersService.GetSubscriptionersOfferId(phoneNumber);
            var offer = _offersService.GetOffer(id);

            var subscriptionersOffer = _offersService.GetSubscriptionersOffer(phoneNumber);

            if (subscriptionersOffer.TimeOfAllConnections > offer.PackageOfFreeMinutes * 60)
                return true;
            else
                return false;
        }

        public bool ComparisonStateOfSmsInOffer(string phoneNumber)
        {
            var id = _offersService.GetSubscriptionersOfferId(phoneNumber);
            var offer = _offersService.GetOffer(id);

            var subscriptionersOffer = _offersService.GetSubscriptionersOffer(phoneNumber);

            if (subscriptionersOffer.AmmountOfAllMessage > offer.PackageOfFreeSms)
                return true;
            else
                return false;
        }
    }
}