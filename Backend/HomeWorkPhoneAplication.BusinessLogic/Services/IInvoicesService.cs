﻿using System.Collections.Generic;
using HomeWorkPhoneAplication.BusinessLogic.Models;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public interface IInvoicesService
    {
        InvoiceBl AddInvoice(InvoiceBl invoiceBl);
        decimal CalculateBaseCostForOffers(int Id);
        decimal CalculateExtraConnectionsCostForOffers(int Id);
        decimal CalculateExtraSmsCostForOffers(int Id);
        void ExportToFile(string path, InvoiceBl invoiceBl, string fileFormat);
        InvoiceBl GenerateInvoice(int subscriptionerId);
        string GenerateNameForInvoiceFile(string numberOfInvoice, string fileFormat);
        string GenerateNumberOfInvoice(int subscriptionerId);
        int GetIdOfLastInvoiceSelectedSubscriptioners(int id);
        List<InvoiceBl> GetListOfSubscriptionersInvoices(int Id);
        InvoiceBl GetSelectedInvoice(string numberOfInvoice);
        void ImportFromFile();
        decimal TotalCostForPeriod(decimal a, decimal b, decimal c);
    }
}