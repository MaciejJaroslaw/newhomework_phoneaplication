﻿using System;
using System.Collections.Generic;
using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.DataLogic.Models;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public interface IOffersService
    {
        int AddOffer(OfferBl offerBl);
        string CreateNewNumber();
        List<OfferBl> GetAll();
        decimal GetCostOfConnection(int offerId);
        decimal GetCostOfMessage(string phoneNumber);
        OfferBl GetOffer(int Id);
        SubscriptionersOfferBl GetSubscriptionersOffer(string phoneNumber);
        int GetSubscriptionersOfferId(string phoneNumber);
        void OverwriteStateOfOffer(string phoneNumber, TimeSpan talkTime);
        void OverwriteStateOfSmsInOffer(string phoneNumber);
        void UpdateSubscriptionersOffer(SubscriptionersOffer subscriptionersOffer);
    }
}