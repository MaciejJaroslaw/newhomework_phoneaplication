﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.DataLogic;
using HomeWorkPhoneAplication.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.BusinessLogic.Services
{
    public class OffersService : IOffersService
    {
        private Random _generator = new Random();
        IDataObjectMapper _dataObjectsMapper = new DataObjectMapper();

        public int AddOffer (OfferBl offerBl)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var offer = dbContext.OffersDbSet.Add(_dataObjectsMapper.MapOfferBlToOffer(offerBl));
                dbContext.SaveChanges();
                return offer.Id;
            }

        }

        public List<OfferBl> GetAll()
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var list = dbContext.OffersDbSet;
                var listBl = new List<OfferBl>();

                foreach (Offer offer in list)
                {
                    listBl.Add(_dataObjectsMapper.MapOfferToOfferBl(offer));
                }
                return listBl;
            }
        }

        public OfferBl GetOffer(int Id)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
               var offer = dbContext.OffersDbSet.SingleOrDefault(x => x.Id == Id);
               var offerBl = _dataObjectsMapper.MapOfferToOfferBl(offer);

                return offerBl;
            }
        }

        public int GetSubscriptionersOfferId(string phoneNumber)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var subscriptionersOffer = dbContext.SubscriptionersOffersDbSet.SingleOrDefault(y => y.NumberOfPhone == phoneNumber);
                int Id = subscriptionersOffer.IdOfOffer;

                return Id;
            }
        }

        public SubscriptionersOfferBl GetSubscriptionersOffer(string phoneNumber)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                var subscriptionersOffer = dbContext.SubscriptionersOffersDbSet.SingleOrDefault(y => y.NumberOfPhone == phoneNumber);
                var subscriptionersOfferBl = _dataObjectsMapper.MapSubscriptionersOfferToSubscriptionersOfferBl(subscriptionersOffer);

                return subscriptionersOfferBl;
            }
        }

        public string CreateNewNumber()
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                while (true)
                {
                    int NewNumberOfPhone = _generator.Next(750000000, 799999999);
                    string numberOfPhone = NewNumberOfPhone.ToString();
                    if (!dbContext.SubscriptionersOffersDbSet.Any(x => x.NumberOfPhone == numberOfPhone))
                    {
                        return numberOfPhone;
                    }
                }
            }
        }

        public decimal GetCostOfConnection(int offerId)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
              var offer = dbContext.OffersDbSet.SingleOrDefault(x => x.Id == offerId);
              return offer.CostOfMinute;
            }
        }

        public decimal GetCostOfMessage(string phoneNumber)
        {
            var Id = GetSubscriptionersOfferId(phoneNumber);
            var offer = GetOffer(Id);
            var costOfSms = offer.CostOfSms;

            return costOfSms;
        }

        public void OverwriteStateOfOffer(string phoneNumber, TimeSpan talkTime)
        {
            var thisOffer = GetSubscriptionersOffer(phoneNumber);
            thisOffer.TimeOfAllConnections += (int)talkTime.TotalSeconds;

            UpdateSubscriptionersOffer(_dataObjectsMapper.MapSubscriptionersOfferBlToSubscriptionerOffer(thisOffer));
        }

        public void OverwriteStateOfSmsInOffer(string phoneNumber)
        {
            var thisOffer = GetSubscriptionersOffer(phoneNumber);
            thisOffer.AmmountOfAllMessage++;

            UpdateSubscriptionersOffer(_dataObjectsMapper.MapSubscriptionersOfferBlToSubscriptionerOffer(thisOffer));
        }

        public void UpdateSubscriptionersOffer(SubscriptionersOffer subscriptionersOffer)
        {
            using (var dbContext = new HomeWorkPhoneAplicationDbContext())
            {
                dbContext.SubscriptionersOffersDbSet.Attach(subscriptionersOffer);
                dbContext.Entry(subscriptionersOffer).State = EntityState.Modified;
                dbContext.SaveChanges();
            }
        }
    }
}
