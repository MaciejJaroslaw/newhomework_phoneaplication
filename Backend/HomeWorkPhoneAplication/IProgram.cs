﻿namespace HomeWorkPhoneAplication
{
    interface IProgram
    {
        void Run();
        void AssignOfferToCustomer();
        int ChoiceOfSubscriptionersId();
        void CreateNewOffer();
        void CreateNewSubscriptioner();
        void Exit();
        void ExportInvoiceToFile();
        void GenerateInvoice();
    }
}