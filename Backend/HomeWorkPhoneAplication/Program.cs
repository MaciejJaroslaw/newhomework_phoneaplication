﻿using HomeWorkPhoneAplication.BusinessLogic.Models;
using HomeWorkPhoneAplication.BusinessLogic.Services;
using HomeWorkPhoneAplication.Helpers;
using HomeWorkPhoneAplication.Tools.FileSerializer;
using Ninject;
using System;
using System.Collections.Generic;

namespace HomeWorkPhoneAplication
{
    class Program : IProgram
    {
        private IOffersService _offersService;
        private ISubscriptionersService _subscriptionersService;
        private IInvoicesService _invoiceService;
        private ChangerFilesFormat _changerFilesFortmat = new ChangerFilesFormat();
        private IPhonesService _phoneService;
        private Menu _menu = new Menu();
        private bool quit;

        public Program(IOffersService offersService, ISubscriptionersService subscriptionersService,
        IInvoicesService invoicesService, IPhonesService phonesService)
        {
            _offersService = offersService;
            _phoneService = phonesService;
            _subscriptionersService = subscriptionersService;
            _invoiceService = invoicesService;
        }

        static void Main(string[] args)
        {
            IKernel nInjectContainer = new StandardKernel();
            nInjectContainer.Bind<IOffersService>().To<OffersService>();
            nInjectContainer.Bind<IPhonesService>().To<PhonesService>();
            nInjectContainer.Bind<ISubscriptionersService>().To<SubscriptionersService>();
            nInjectContainer.Bind<IInvoicesService>().To<InvoicesService>();
            nInjectContainer.Bind<IProgram>().To<Program>();

            nInjectContainer.Get<IProgram>().Run();
        }

        public void Run()
        {
            _menu.AddCommand("Exit", Exit);
            _menu.AddCommand("Add new Subscriptioner", CreateNewSubscriptioner);
            _menu.AddCommand("Add new Offer", CreateNewOffer);
            _menu.AddCommand("assign an offer to the customer", AssignOfferToCustomer);
            _menu.AddCommand("generate a invoice", GenerateInvoice);
            _menu.AddCommand("Export Invoice to File", ExportInvoiceToFile);


            while(!quit)
            {
                _menu.PrintAllCommands();
                var command = IoHelper.GetStringFromUser("Select command");
                _menu.RunCommand(command);
            }
        }

        public void Exit()
        {
            quit = true;
        }

        public void CreateNewSubscriptioner()
        {
            var newSubscriptioner = new SubscriptionerBl
            {
                Name = IoHelper.GetStringFromUser("Enter the name of customer"),
                Surname = IoHelper.GetStringFromUser("Enter the surname of customer"),
                DateOfBorn = IoHelper.GetDataFromUser("Enter the date of born: dd-mm-yyyy"),
                Email = AddEmailToSubscriptioner(),
                BillingPeriod = _subscriptionersService.GenerateBillingPeriod()
            };

            Console.WriteLine($"Your billing period is {newSubscriptioner.BillingPeriod}th of every mont");
            _subscriptionersService.AddSubscriptioner(newSubscriptioner);
        }

        public void CreateNewOffer()
        {
            var newOffer = new OfferBl
            {
                Name = IoHelper.GetStringFromUser("Enter the name of offer"),
                CostOfOnePeriod = IoHelper.GetDecimalFromUser("Enter prize for one month in this offer"),
                PackageOfFreeMinutes = IoHelper.GetIntFromUser("How many free minutes include this offer?"),
                PackageOfFreeSms = IoHelper.GetIntFromUser("How many free sms include this offer?"),
                CostOfMinute = IoHelper.GetDecimalFromUser("Enter prize for one minute out of offer"),
                CostOfSms = IoHelper.GetDecimalFromUser("Enter prize for one sms out of offer"),
            };

            _offersService.AddOffer(newOffer);
        }

        public void AssignOfferToCustomer()
        {
            int subscriptionerId = ChoiceOfSubscriptionersId();

            var offers = _offersService.GetAll();
            foreach (OfferBl offerBl in offers)
            {
                Console.WriteLine($"offer {offerBl.Name} for {offerBl.CostOfOnePeriod} pln/one month (Id of this offer {offerBl.Id})");
            }
            var offerId = IoHelper.GetIntFromUser("Select Id of Offer");
            var offer = _offersService.GetOffer(offerId);
            
            var numberOfPhone = _subscriptionersService.SubscribeNewOffer(subscriptionerId, offerId, offer.CostOfOnePeriod);

            Console.WriteLine($"The new number of phone for this subscriptioner is {numberOfPhone}");
        }

        public void GenerateInvoice()
        {
            int subscriptionerId = ChoiceOfSubscriptionersId();

            var invoiceBl = _invoiceService.GenerateInvoice(subscriptionerId);

            _invoiceService.AddInvoice(invoiceBl);
            _subscriptionersService.StartAnewBillingPeriod(subscriptionerId);
        }

        public void ExportInvoiceToFile()
        {
            int Id = ChoiceOfSubscriptionersId();

            List<InvoiceBl> invoicesBl = _invoiceService.GetListOfSubscriptionersInvoices(Id);
            foreach (InvoiceBl invoiceBl in invoicesBl)
            {
                Console.WriteLine($"Invoice with number: {invoiceBl.NumberOfInvoice} Include cost:");
                Console.WriteLine($"{invoiceBl.StandardCostOfAllOffers} pln for Basic services, cost {invoiceBl.TotalCostOfExtraMessages}" +
                    $" pln for extra messages and cost {invoiceBl.TotalCostOfExtraConnections} pln for extra conncections.");
                Console.WriteLine($"Total cost for this period is {invoiceBl.TotalCostForPeriod} pln");
                Console.WriteLine("\n");
            }

            var invoiceToGet = IoHelper.GetStringFromUser("Select Invoice to export (enter number of selected invoice)");
            var invoiceToExport = _invoiceService.GetSelectedInvoice(invoiceToGet);

            Console.WriteLine("Choose FileManager");
            List<string> formats = _changerFilesFortmat.GetSupportedFormats();
            
            foreach (String format in formats)
            {
                Console.WriteLine(format);
            }

            var managerName = Console.ReadLine();
            var fileFormat = _changerFilesFortmat.GetSerializer(managerName);
            var fileFormatName = fileFormat.GetFileFormat();       
            var path = IoHelper.GetStringFromUser("Enter path to write an invoice");

            _invoiceService.ExportToFile(path, invoiceToExport, fileFormatName);
        }

        public int ChoiceOfSubscriptionersId()
        {
            var subscriptioners = _subscriptionersService.GetAll();
            foreach (SubscriptionerBl subscriptionerBl in subscriptioners)
            {
                Console.WriteLine($"Name: {subscriptionerBl.Name} Surname: {subscriptionerBl.Surname} with Id: {subscriptionerBl.Id}");
            }
            var subscriptionerId = IoHelper.GetIntFromUser("Select Id of subscriptioner");

            return subscriptionerId;
        }

        public string AddEmailToSubscriptioner()
        {
            var email = IoHelper.GetStringFromUser("Enter the adres E-mail");
            var correctness = _subscriptionersService.CheckTheEmail(email);
            if (correctness == true)
            {
                return email;
            }
            else
                Console.WriteLine($"{email} is not corrected e-mail");
                email = AddEmailToSubscriptioner();

            return email;
        }
    }
}
