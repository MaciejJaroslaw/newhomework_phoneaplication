﻿using HomeWorkPhoneAplication.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.DataLogic
{
    public class HomeWorkPhoneAplicationDbContext : DbContext
    {
        public HomeWorkPhoneAplicationDbContext() : base(GetConnectionString())
        { }

        public DbSet<Subscriptioner>SubscriptionersDbSet { get; set; }
        public DbSet<Offer>OffersDbSet { get; set; }
        public DbSet<SubscriptionersOffer>SubscriptionersOffersDbSet { get; set; }
        public DbSet<Activities>ActivitiesDbSet { get; set; }
        public DbSet<Invoice>InvoicesDbSet { get; set; }
        public DbSet<DetailsInInvoice> DetailsInInvoicesDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["HomeWorkPhoneAplicationDbConnectionString"].ConnectionString;
        }
    }
}