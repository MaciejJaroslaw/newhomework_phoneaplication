﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.DataLogic.Models
{
    public class Activities
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string KindOfAction { get; set; }
        public TimeSpan ConnectionTime { get; set; }
        public DateTime TimeOfRealization { get; set; }
        public SubscriptionersOffer SubscribersOffer { get; set; }
    }
}
