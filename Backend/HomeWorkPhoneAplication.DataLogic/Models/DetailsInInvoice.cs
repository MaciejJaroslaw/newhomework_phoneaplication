﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.DataLogic.Models
{
    public class DetailsInInvoice
    {
        public int IdOfYourOffer { get; set; }
        public decimal BaseCost { get; set; }
        public decimal CostOfExtraSms { get; set; }
        public decimal CostOfExtraConnections { get; set; }
    }
}
