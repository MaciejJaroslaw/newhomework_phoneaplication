﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.DataLogic.Models
{
    public class Subscriptioner
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public int BillingPeriod { get; set; }
        public DateTime DateOfBorn { get; set; }
    }
}
