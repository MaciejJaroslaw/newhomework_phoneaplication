﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.DataLogic.Models
{
    public class SubscriptionersOffer
    {
        public int Id { get; set; }
        public int IdOfOffer { get; set; }
        public int IdOfSubscriptioner { get; set; }
        public string NumberOfPhone { get; set; }
        public int TimeOfAllConnections { get; set; }
        public int AmmountOfAllMessage { get; set; }
        public decimal CostOfOffer { get; set; }

        public List<Activities> Activities { get; set; } = new List<Activities>();
    }
}
