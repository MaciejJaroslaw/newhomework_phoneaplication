﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.DataLogic.Models
{
    public class Offer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PackageOfFreeMinutes { get; set; }
        public int PackageOfFreeSms { get; set; }
        public decimal CostOfMinute { get; set; }
        public decimal CostOfSms { get; set; }
        public decimal CostOfOnePeriod { get; set; }
    }
}
