﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.DataLogic.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public int IdOfSubscriptioners { get; set; }
        public string NumberOfInvoice { get; set; }
        public decimal StandardCostOfAllOffers { get; set; }
        public decimal TotalCostOfExtraConnections { get; set; }
        public decimal TotalCostOfExtraMessages { get; set; }
        public decimal TotalCostForPeriod { get; set; }

        public List<DetailsInInvoice> details = new List<DetailsInInvoice>();
    }
}
