﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.Helpers
{
    public class IoHelper
    {
        public static string GetStringFromUser(string message)
        {
            Console.WriteLine($"{message}: ");
            return Console.ReadLine();
        }

        public static int GetIntFromUser(string message)
        {
            Console.WriteLine($"{message}: ");
            return int.Parse(Console.ReadLine());
        }

        public static DateTime GetDataFromUser(string message)
        {
            Console.WriteLine($"{message}: ");
            return DateTime.Parse(Console.ReadLine());
        }

        public static decimal GetDecimalFromUser(string message)
        {
            Console.WriteLine($"{message}: ");
            return decimal.Parse(Console.ReadLine());
        }
    }
}
