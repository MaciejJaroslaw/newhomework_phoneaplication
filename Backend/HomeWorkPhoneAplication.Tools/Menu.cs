﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication
{
    public class Menu
    {
        private Dictionary<string, Action> _actionsDictionary = new Dictionary<string, Action>();

        public void AddCommand(string name, Action action)
        {
            _actionsDictionary.Add(name, action);
        }

        public void PrintAllCommands()
        {
            Console.WriteLine("available commands");

            foreach (string name in _actionsDictionary.Keys)
            {
                Console.WriteLine($" * {name}");
            }
        }

        public void RunCommand(string name)
        {
            try
            {
                _actionsDictionary[name]();
            }
            catch (Exception e)
            {
                Console.WriteLine($">{e.Message}");
            }
        }
    }
}
