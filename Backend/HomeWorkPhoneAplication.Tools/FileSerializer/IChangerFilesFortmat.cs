﻿using System.Collections.Generic;

namespace HomeWorkPhoneAplication.Tools.FileSerializer
{
    public interface IChangerFilesFormat
    {
        ISerializator GetSerializer(string managerName);
        List<string> GetSupportedFormats();
    }
}