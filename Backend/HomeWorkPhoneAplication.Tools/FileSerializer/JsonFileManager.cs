﻿using HomeWorkPhoneAplication.DataLogic.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.Tools.FileSerializer
{
    public class JsonFileManager : ISerializator
    {
        public const string FileExtension = "json";
        public string GetFileFormat()
        {
            return FileExtension;
        }

        public void SaveToFile(string path, string fileName, Invoice invoice)
        {
            var json = JsonConvert.SerializeObject(invoice, Formatting.Indented);

            var filePath = Path.Combine(path, fileName);

            File.WriteAllText(filePath, json);
        }

        public Invoice ReadFromFile(string fileName)
        {
            var json = File.ReadAllText(fileName);
            return JsonConvert.DeserializeObject<Invoice>(json);
        }
    }
}
