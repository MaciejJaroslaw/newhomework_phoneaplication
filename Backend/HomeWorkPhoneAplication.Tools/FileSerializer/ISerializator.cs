﻿using HomeWorkPhoneAplication.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.Tools.FileSerializer
{
    public interface ISerializator
    {
        void SaveToFile(string path, string fileName, Invoice invoice);
        Invoice ReadFromFile(string fileName);
        string GetFileFormat();
    }
}
