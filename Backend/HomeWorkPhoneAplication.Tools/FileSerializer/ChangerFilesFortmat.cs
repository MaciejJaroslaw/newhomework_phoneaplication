﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWorkPhoneAplication.Tools.FileSerializer
{
    public class ChangerFilesFormat : IChangerFilesFormat
    {
        public ISerializator GetSerializer(string managerName)
        {
            switch (managerName.ToLower())
            {
                case XMLFileManager.FileExtension:
                    return new XMLFileManager();
                case JsonFileManager.FileExtension:
                    return new JsonFileManager();
                default:
                    return null;
            }
        }

        public List<string> GetSupportedFormats()
        {
            List<string> formats = new List<string>();
            formats.Add(XMLFileManager.FileExtension);
            formats.Add(JsonFileManager.FileExtension);

            return formats;
        }
    }
}
