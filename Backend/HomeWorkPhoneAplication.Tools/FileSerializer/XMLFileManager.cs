﻿using HomeWorkPhoneAplication.DataLogic.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HomeWorkPhoneAplication.Tools.FileSerializer
{
    public class XMLFileManager : ISerializator
    {
        public const string FileExtension = "xml";
        public string GetFileFormat()
        {
            return FileExtension;
        }

        public void SaveToFile(string path, string fileName, Invoice invoice)
        {
            var serializer = new XmlSerializer(typeof(Invoice));
            var filePath = Path.Combine(path, fileName);

            using (var writer = new StreamWriter(filePath))
            {
                serializer.Serialize(writer, invoice);
            }
        }

        public Invoice ReadFromFile(string fileName)
        {
            var serializer = new XmlSerializer(typeof(Invoice));
            using (var reader = new StreamReader(fileName))
            {
                return (Invoice)serializer.Deserialize(reader);
            }
        }
    }
}
